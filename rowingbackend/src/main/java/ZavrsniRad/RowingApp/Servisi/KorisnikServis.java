package ZavrsniRad.RowingApp.Servisi;

import ZavrsniRad.RowingApp.Modeli.Korisnik;
import ZavrsniRad.RowingApp.Repozitoriji.KorisnikRepozitorij;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;


@Service
public class KorisnikServis{

    @Autowired
    KorisnikRepozitorij korisnikRepo;

    public void dodavanjeNovogKorisnika(Korisnik korisnik){
        try{
            if(korisnikRepo.findById(korisnik.getId()).get().getEmail().equals(korisnik.getEmail())) { //Provjerava postoji li već korisnik sa unesenom adresom


                //Throw exception!!!!!!!!!!!!!!!!!!!!
            }else{
                korisnikRepo.save(korisnik);
            }

        }catch(NoSuchElementException ex){
            korisnikRepo.save(korisnik);
        }
    }

    public List<Korisnik> dohvatiKorisnike(){
        return korisnikRepo.findAll();
    }

    public Korisnik dohvatiKorisnika(int idKorisnika){
        return korisnikRepo.findById(idKorisnika).get();
    }
	
	public int dohvatiId(String email){
		return korisnikRepo.findByEmail(email).getId();
	}

	public void azurirajVrijemeTest(Korisnik korisnik){
        Korisnik korisnikAzurirano=korisnikRepo.findById(korisnik.getId()).get();
        korisnikAzurirano.setVrijemeTest(korisnik.getVrijemeTest());
        korisnikRepo.save(korisnikAzurirano);
    }

}
