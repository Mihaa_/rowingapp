package ZavrsniRad.RowingApp.Servisi;

import ZavrsniRad.RowingApp.Modeli.Clanstvo;
import ZavrsniRad.RowingApp.Modeli.Grupa;
import ZavrsniRad.RowingApp.Modeli.Korisnik;
import ZavrsniRad.RowingApp.Repozitoriji.ClanstvoRepozitorij;
import ZavrsniRad.RowingApp.Repozitoriji.GrupaRepozitorij;
import ZavrsniRad.RowingApp.Repozitoriji.KorisnikRepozitorij;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@Service
public class GrupaServis {

    @Autowired
    GrupaRepozitorij grupaRepo;
    @Autowired
    ClanstvoRepozitorij clanRepo;
    @Autowired
    KorisnikRepozitorij korRepo;

    public void dodajGrupu(Grupa grupa){
        grupaRepo.save(grupa);
        Clanstvo clanstvo=new Clanstvo(grupa.getIdVlasnik(),grupa.getId());
        clanRepo.save(clanstvo);
    }

    public List<Grupa> dohvatiGrupe(){
        return grupaRepo.findAll();
    }

    public List<Grupa> dohvatiMojeGrupe(int idPozivatelja){
        List<Grupa> mojeGrupe=new LinkedList<>();
        for(Grupa grupa : grupaRepo.findAll()){
            if(grupa.getIdVlasnik()==idPozivatelja)
                mojeGrupe.add(grupa);
        }
        return mojeGrupe;
    }

    public List<Grupa> dohvatiUclanjeneGrupe(int idPozivatelja){
        List<Grupa> uclanjeneGrupe= new LinkedList<>();
        List<Clanstvo> clanstva=clanRepo.findAll();

        for(Clanstvo clanstvo: clanstva){
            if(clanstvo.getIdKorisnik()== idPozivatelja)
                uclanjeneGrupe.add(grupaRepo.findById(clanstvo.getIdGrupa()).get());
        }
        return uclanjeneGrupe;
    }

    public void obrisiKorisnikaIzGrupe(Clanstvo clanstvo){
        clanRepo.delete(clanstvo);
    }

    public List<Korisnik> dohvatiClanoveGrupe(int idGrupe){
        List<Korisnik> clanovi=new LinkedList<>();
        List<Clanstvo> clanstva=clanRepo.findAll();

        for(Clanstvo clanstvo: clanstva){
            if(clanstvo.getIdGrupa()==idGrupe)
                clanovi.add(korRepo.findById(clanstvo.getIdKorisnik()).get());
        }
        return clanovi;
    }
	
	public Grupa dohvatiGrupu(int idGrupa){
		return grupaRepo.findById(idGrupa).get();
	}

    public List<Korisnik> poredakUGrupi(int idGrupe){
        List<Korisnik> poredak=dohvatiClanoveGrupe(idGrupe);
        Collections.sort(poredak);
        return poredak;
    }
}
