package ZavrsniRad.RowingApp.Servisi;

import ZavrsniRad.RowingApp.Modeli.Vjezba;
import ZavrsniRad.RowingApp.Repozitoriji.VjezbaRepozitorij;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VjezbaServis {

    @Autowired
    VjezbaRepozitorij vjezbaRepo;

    public void dodajVjezbu(Vjezba vjezba){
        vjezbaRepo.save(vjezba);
    }

    public List<Vjezba> dohvatiVjezbe(){
        return vjezbaRepo.findAll();
    }

    public Vjezba dohvatiVjezbu(int idVjezbe){
        return vjezbaRepo.findById(idVjezbe).get();
    }
}
