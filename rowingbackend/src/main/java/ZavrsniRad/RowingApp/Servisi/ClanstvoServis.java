package ZavrsniRad.RowingApp.Servisi;

import ZavrsniRad.RowingApp.Modeli.Clanstvo;
import ZavrsniRad.RowingApp.Modeli.Korisnik;
import ZavrsniRad.RowingApp.Repozitoriji.ClanstvoRepozitorij;
import ZavrsniRad.RowingApp.Repozitoriji.KorisnikRepozitorij;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@Service
public class ClanstvoServis {

    @Autowired
    ClanstvoRepozitorij clanstvoRepo;

    @Autowired
    KorisnikRepozitorij korisnikRepozitorij;

    public void dodajClanstvo(Clanstvo clanstvo){
        clanstvoRepo.save(clanstvo);
    }

    public List<Clanstvo> dohvatiClanstva(){
       return clanstvoRepo.findAll();
    }

    public List<Korisnik> dohvatiClanove(int idGrupe) {
        List<Clanstvo> svaClanstva=clanstvoRepo.findAll();
        List<Clanstvo> clanstva=new LinkedList<>();
        List<Korisnik> clanovi=new LinkedList<>();
        for(Clanstvo clanstvo:svaClanstva){
            if(clanstvo.getIdGrupa()==idGrupe){
                clanstva.add(clanstvo);
            }
        }

        for(Clanstvo clanstvo:clanstva){
            Korisnik korisnik=korisnikRepozitorij.findById(clanstvo.getIdKorisnik()).get();
            korisnik.setLozinka("");
            clanovi.add(korisnik);
        }
        Collections.sort(clanovi);
        return clanovi;

    }
}
