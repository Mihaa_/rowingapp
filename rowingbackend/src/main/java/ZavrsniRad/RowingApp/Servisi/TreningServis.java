package ZavrsniRad.RowingApp.Servisi;

import ZavrsniRad.RowingApp.Modeli.GrafPrikaz;
import ZavrsniRad.RowingApp.Modeli.Trening;
import ZavrsniRad.RowingApp.Modeli.TreningVjezba;
import ZavrsniRad.RowingApp.Modeli.Vjezba;
import ZavrsniRad.RowingApp.Repozitoriji.TreningRepozitorij;
import ZavrsniRad.RowingApp.Repozitoriji.VjezbaRepozitorij;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@Service
public class TreningServis {

    private final int brojZadnjihDistanci=20;

    @Autowired
    TreningRepozitorij treningRepo;
    @Autowired
    VjezbaRepozitorij vjezbaRepo;

    public void dodajTrening(Trening trening){
        treningRepo.save(trening);
    }

    public List<Trening> dohvatiTreninge(){
        return treningRepo.findAll();
    }

    public List<Trening> dohvatiMojeTreninge(int idPozivatelja){
        List<Trening> mojiTreninzi=new LinkedList<>();
        List<Trening> sviTreninzi=treningRepo.findAll();
        Collections.sort(sviTreninzi);
        for(Trening trening:sviTreninzi){
            if(trening.getIdKorisnika()==idPozivatelja){
                mojiTreninzi.add(trening);
            }
        }
        return mojiTreninzi;
    }

    public List<Trening> dohvatiTreningVjezba(int idPozivatelja, int idVjezbe){
        List<Trening> mojiTreninzi=dohvatiMojeTreninge(idPozivatelja);
        List<Trening> mojTreningVjezba=new LinkedList<>();

        for(Trening trening:mojiTreninzi){
            if(trening.getIdVjezba()==idVjezbe)
                mojTreningVjezba.add(trening);
        }
        return mojTreningVjezba; //lista treninga za odabranu vjezbu
    }

    public List<GrafPrikaz> dohvatiZadnjeDistance(int idPozivatelja){//prikaz treninga
       return prikaziTreninge(dohvatiMojeTreninge(idPozivatelja));
    }

    public List<GrafPrikaz> dohvatiZadnjeDistanceZaPojedinuVjezbu(int idPozivatelja,int idVjezbe){ //prikaz treninga za pojedinu vjezbu
        return prikaziTreninge(dohvatiTreningVjezba(idPozivatelja,idVjezbe));
    }

    public List<GrafPrikaz> prikaziTreninge(List<Trening> treninzi){ //Na temelju predane liste stvara podatke za prikaz
        List<GrafPrikaz> listaPrikaza=new LinkedList<>();
        int brojZapisa = 0;

        for(Trening trening:treninzi){
            List<Double> ocekVrijeme=vjezbaRepo.findById(trening.getIdVjezba()).get().getOcekivanoVrijeme();

            for(int i=0;i<ocekVrijeme.size();i++){ //postavljamo ocekivano vrijeme
               ocekVrijeme.set(i,ocekVrijeme.get(i)+trening.getVrijemeTest());
            }

            List<Double> rez=trening.getRezultat();

            for(int i=0; i< rez.size();i++){
                if (brojZapisa>20)
                    break;
                listaPrikaza.add(new GrafPrikaz(ocekVrijeme.get(i),rez.get(i),i));
                brojZapisa++;
            }
            if (brojZapisa>20)
                break;

        }
        return listaPrikaza;
    }

    public TreningVjezba dohvatiTrening(int idTrening){
        Trening trening=treningRepo.findById(idTrening).get();
        Vjezba vjezba= vjezbaRepo.findById(trening.getIdVjezba()).get();
        List<Double> pomOcekivano=new LinkedList<>();
        for(int i=0; i<vjezba.getOcekivanoVrijeme().size();i++){
            pomOcekivano.add(vjezba.getOcekivanoVrijeme().get(i) + trening.getVrijemeTest());
        }
        return new TreningVjezba(vjezba.getNaziv(),vjezba.getDistance(),trening.getDatum(),trening.getVrijemeTest(),trening.getRezultat(),vjezba.getOcekivanoVrijeme(),vjezba.getNapomena());
    }

}
