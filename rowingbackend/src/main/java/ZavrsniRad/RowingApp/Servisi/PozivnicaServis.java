package ZavrsniRad.RowingApp.Servisi;

import ZavrsniRad.RowingApp.Modeli.Clanstvo;
import ZavrsniRad.RowingApp.Modeli.Grupa;
import ZavrsniRad.RowingApp.Modeli.Pozivnica;
import ZavrsniRad.RowingApp.Repozitoriji.ClanstvoRepozitorij;
import ZavrsniRad.RowingApp.Repozitoriji.GrupaRepozitorij;
import ZavrsniRad.RowingApp.Repozitoriji.PozivnicaRepozitorij;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class PozivnicaServis {

    @Autowired
    PozivnicaRepozitorij pozivnicaRepo;
    @Autowired
    GrupaRepozitorij grupaRepo;
    @Autowired
    ClanstvoRepozitorij clanRepo;

    public void dodajPozivnicu(Pozivnica pozivnica, int idPozivatelja){
        Grupa grupa=grupaRepo.findById(pozivnica.getIdGrupa()).get();

        if(grupa.isPrivatnost()==false|| grupa.getIdVlasnik()==idPozivatelja){ //Provjerava da li je korisnik koji salje pozivnicu ujedno i vlasnik grupe za koju salje pozivnicu
           pozivnicaRepo.save(pozivnica);
        }else{
            //THROW EXCEPTION!!!!!!!!!!!!!!!!!!!!!!!!!!1
        }
    }

    public void prihvatiPozivnicu(Pozivnica pozivnica, int idPozivatelja){
        if(pozivnica.getIdKorisnik()!=idPozivatelja){ //Provjerava da li je pozivnica koju korisnik prihvaca stvarno namjenjena za njega
            //THROW EXCEPTION!!!!!!!!!!!!!!!!!!!!!!
        }
        Clanstvo clanstvo= new Clanstvo(pozivnica.getIdKorisnik(),pozivnica.getIdGrupa());
        clanRepo.save(clanstvo);
        pozivnicaRepo.delete(pozivnica);
    }

    public List<Pozivnica> dohvatiMojePozivnice(int idPozivatelja){
        List<Pozivnica> mojePozivnice=new LinkedList<>();
        List<Pozivnica> pozivnice=pozivnicaRepo.findAll();

        for(Pozivnica pozivnica: pozivnice){
            if(pozivnica.getIdKorisnik()==idPozivatelja)
                mojePozivnice.add(pozivnica);
        }
        return mojePozivnice;
    }

    public void odbijPozivnicu(Pozivnica pozivnica){
        pozivnicaRepo.delete(pozivnica);
    }

    public List<Pozivnica> dohvatiPozivnice(){
        return pozivnicaRepo.findAll();
    }
}
