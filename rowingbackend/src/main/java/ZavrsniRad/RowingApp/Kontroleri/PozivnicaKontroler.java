package ZavrsniRad.RowingApp.Kontroleri;

import ZavrsniRad.RowingApp.Modeli.GrupPoz;
import ZavrsniRad.RowingApp.Modeli.Pozivnica;
import ZavrsniRad.RowingApp.Repozitoriji.GrupaRepozitorij;
import ZavrsniRad.RowingApp.Servisi.PozivnicaServis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping("/pozivnica")
public class PozivnicaKontroler {

    @Autowired
    PozivnicaServis pozivnicaServis;
    @Autowired
    GrupaRepozitorij grupaRepozitorij;

    @PostMapping("/dodaj/{pozivatelj}")
    public void dodajPozivnicu(@RequestBody Pozivnica pozivnica,@PathVariable("pozivatelj") int idPozivatelja){
        pozivnicaServis.dodajPozivnicu(pozivnica, idPozivatelja);
    }

    @PostMapping("/prihvati/{pozivatelj}")
    public void prihvatiPozivnicu(@RequestBody Pozivnica pozivnica, @PathVariable("pozivatelj") int idPozivatelja ){
        pozivnicaServis.prihvatiPozivnicu(pozivnica,idPozivatelja);
    }

    @PostMapping("/odbij")
    public void odbijPozivnicu(@RequestBody Pozivnica pozivnica){
        pozivnicaServis.odbijPozivnicu(pozivnica);
    }

    @GetMapping("/dohvati")
    public List<Pozivnica> dohvatiPozivnice(){
        return pozivnicaServis.dohvatiPozivnice();
    }

    @GetMapping("/dohvati/MojePozivnice/{pozivatelj}")
    public List<GrupPoz> dohvatiMojePozivnicie(@PathVariable("pozivatelj") int idPozivatelja){
        List<Pozivnica> pozivnice= pozivnicaServis.dohvatiMojePozivnice(idPozivatelja);
        List<GrupPoz> grupPozs= new LinkedList<>();

        for(Pozivnica pozivnica: pozivnice){
            grupPozs.add(new GrupPoz(grupaRepozitorij.findById(pozivnica.getIdGrupa()).get(),pozivnica));

        }
        return grupPozs;
    }


}


