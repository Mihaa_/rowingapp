package ZavrsniRad.RowingApp.Kontroleri;

import ZavrsniRad.RowingApp.Modeli.Clanstvo;
import ZavrsniRad.RowingApp.Modeli.Korisnik;
import ZavrsniRad.RowingApp.Servisi.ClanstvoServis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/clanstvo")
public class ClanstvoKontroler {

    @Autowired
    ClanstvoServis clanstvoServis;

    @PostMapping("/dodaj")
    public void dodajClanstvo(@RequestBody Clanstvo clanstvo){
        clanstvoServis.dodajClanstvo(clanstvo);
    }

    @GetMapping("/dohvati")
    public List<Clanstvo> dohvatiClanstva(){
        return clanstvoServis.dohvatiClanstva();
    }

    @GetMapping("/dohvatiClanove/{idGrupe}")
    public List<Korisnik> dohvatiClanove(@PathVariable(value = "idGrupe") int idGrupe){
        return clanstvoServis.dohvatiClanove(idGrupe);
    }
}
