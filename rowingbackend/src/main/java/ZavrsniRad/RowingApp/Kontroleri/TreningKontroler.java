package ZavrsniRad.RowingApp.Kontroleri;

import ZavrsniRad.RowingApp.Modeli.TrVjezba;
import ZavrsniRad.RowingApp.Modeli.Trening;
import ZavrsniRad.RowingApp.Modeli.TreningVjezba;
import ZavrsniRad.RowingApp.Modeli.Vjezba;
import ZavrsniRad.RowingApp.Repozitoriji.VjezbaRepozitorij;
import ZavrsniRad.RowingApp.Servisi.TreningServis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping("/trening")
public class TreningKontroler {

    @Autowired
    TreningServis treningServis;
    @Autowired
    VjezbaRepozitorij vjezbaRepozitorij;

    @PostMapping("/dodaj")
    public void dodajTrening(@RequestBody Trening trening){
        treningServis.dodajTrening(trening);
    }

    @GetMapping("/dohvati")
    public List<Trening> dohvatiTreninge(){
        return treningServis.dohvatiTreninge();
    }

    @GetMapping("/dohvati/mojiTreninzi/{pozivatelj}")
    public List<TrVjezba> dohvatiMojeTreninge(@PathVariable("pozivatelj") int idPozivatelja){
        List<TrVjezba> pom=new LinkedList<>();
        List<Trening> pomTren=treningServis.dohvatiMojeTreninge(idPozivatelja);
        for(Trening x: pomTren){
            pom.add(new TrVjezba(x,vjezbaRepozitorij.findById(x.getIdVjezba()).get()));
        }
        return pom;
    }

    @GetMapping("/dohvati/mojiTreninzi/{pozivatelj}/vjezba/{vjezba}") //Vraca listu treninga za pojedinu vjezbu
    public List<Trening> dohvatiMojTreningVjezba(@PathVariable("pozivatelj") int idPozivatelja, @PathVariable("vjezba") int idVjezba){
        return treningServis.dohvatiTreningVjezba(idPozivatelja,idVjezba);
    }

    @GetMapping("/dohvati/{idTrening}")
    public TreningVjezba dohvatiTrening(@PathVariable("idTrening") int idTreninga){
        return treningServis.dohvatiTrening(idTreninga);
    }
}
