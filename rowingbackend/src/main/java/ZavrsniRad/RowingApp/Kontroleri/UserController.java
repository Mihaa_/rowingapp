package ZavrsniRad.RowingApp.Kontroleri;


import ZavrsniRad.RowingApp.Modeli.Korisnik;
import ZavrsniRad.RowingApp.Repozitoriji.KorisnikRepozitorij;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserController {

    private KorisnikRepozitorij korisnikRepo;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserController(KorisnikRepozitorij applicationUserRepository,BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.korisnikRepo = applicationUserRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @PostMapping("/sign-up")
    public void signUp(@RequestBody Korisnik user) {
        user.setLozinka(bCryptPasswordEncoder.encode(user.getLozinka()));
        korisnikRepo.save(user);
    }
}
