package ZavrsniRad.RowingApp.Kontroleri;

import ZavrsniRad.RowingApp.Modeli.Korisnik;
import ZavrsniRad.RowingApp.Repozitoriji.KorisnikRepozitorij;
import ZavrsniRad.RowingApp.Servisi.KorisnikServis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/korisnik")
public class KorisnikKontroler {

    @Autowired
    KorisnikServis korisnikServis;
    @Autowired
    KorisnikRepozitorij korisnikRepozitorij;

    @PostMapping("/azuriraj")
    public void azurirajVrijemeTest(@RequestBody Korisnik korisnik){
        korisnikServis.azurirajVrijemeTest(korisnik);
    }

    @PostMapping("/dodaj")
    public void dodvanjeKorisnika(@RequestBody Korisnik korisnik){
        korisnikServis.dodavanjeNovogKorisnika(korisnik);
    }

    @GetMapping("/dohvati")
    public List<Korisnik> dohvatiKorisnike(){
        return korisnikServis.dohvatiKorisnike();
    }

    @GetMapping("/dohvati/{id}")
    public Korisnik dohvatiKorisnika(@PathVariable("id") int idKorisnika){
       return korisnikServis.dohvatiKorisnika(idKorisnika);
    }


	@GetMapping("/dohvatiId/{email}")
    public int dohvatiIdKor(@PathVariable("email") String email){
        System.out.println(email);
        return korisnikRepozitorij.findByEmail(email).getId();
    }
}
