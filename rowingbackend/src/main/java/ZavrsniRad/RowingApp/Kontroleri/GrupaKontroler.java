package ZavrsniRad.RowingApp.Kontroleri;

import ZavrsniRad.RowingApp.Modeli.Grupa;
import ZavrsniRad.RowingApp.Servisi.GrupaServis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/grupa")
public class GrupaKontroler {

    @Autowired
    GrupaServis grupaServis;

    @PostMapping("/dodaj")
    public void gurpaDodaj(@RequestBody Grupa grupa){
        grupaServis.dodajGrupu(grupa);
    }
	
	@GetMapping("/dohvatiGrupu/{idGrupa}")
	public Grupa dohvatiGrupu(@PathVariable("idGrupa") int idGrupa){
		return grupaServis.dohvatiGrupu(idGrupa);
	}

    @GetMapping("/dohvati")
    public List<Grupa> dohvatiGrupe(){
        return grupaServis.dohvatiGrupe();
    }

    @GetMapping("/mojeGrupe/{pozivatelj}")
    public List<Grupa> dohvatiMojeGrupe(@PathVariable("pozivatelj") int idPozivatelja){
        return grupaServis.dohvatiMojeGrupe(idPozivatelja);
    }

    @GetMapping("/uclanjenjeGrupe/{pozivatelj}")
    public List<Grupa> dohvatiUclanjeneGrupe(@PathVariable("pozivatelj") int idPozivatelja){
        return grupaServis.dohvatiUclanjeneGrupe(idPozivatelja);
    }
}
