package ZavrsniRad.RowingApp.Kontroleri;

import ZavrsniRad.RowingApp.Modeli.Vjezba;
import ZavrsniRad.RowingApp.Servisi.VjezbaServis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/vjezba")
public class VjezbaKontroler {

    @Autowired
    VjezbaServis vjezbaServis;

    @PostMapping("/dodaj")
    public void dodajVjezbu(@RequestBody Vjezba vjezba){
        vjezbaServis.dodajVjezbu(vjezba);
    }

    @GetMapping("/dohvati")
    public List<Vjezba> dohvatiVjezbe(){
        return vjezbaServis.dohvatiVjezbe();
    }

    @GetMapping("/dohvati/{idVjezba}")
    public Vjezba dohvatiVjezbu(@PathVariable("idVjezba") int idVjezbe){
        return vjezbaServis.dohvatiVjezbu(idVjezbe);
    }
}


