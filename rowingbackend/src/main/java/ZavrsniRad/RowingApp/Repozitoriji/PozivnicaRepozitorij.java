package ZavrsniRad.RowingApp.Repozitoriji;

import ZavrsniRad.RowingApp.Modeli.Pozivnica;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PozivnicaRepozitorij extends JpaRepository<Pozivnica,Pozivnica> {
}
