package ZavrsniRad.RowingApp.Repozitoriji;

import ZavrsniRad.RowingApp.Modeli.Grupa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GrupaRepozitorij extends JpaRepository<Grupa,Integer> {
}
