package ZavrsniRad.RowingApp.Repozitoriji;

import ZavrsniRad.RowingApp.Modeli.Korisnik;
import org.springframework.data.jpa.repository.JpaRepository;

public interface KorisnikRepozitorij extends JpaRepository<Korisnik,Integer> {
    Korisnik findByEmail(String email);
}
