package ZavrsniRad.RowingApp.Repozitoriji;

import ZavrsniRad.RowingApp.Modeli.Trening;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TreningRepozitorij extends JpaRepository<Trening,Integer> {
}
