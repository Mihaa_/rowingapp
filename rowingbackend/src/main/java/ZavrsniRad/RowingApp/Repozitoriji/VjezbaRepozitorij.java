package ZavrsniRad.RowingApp.Repozitoriji;

import ZavrsniRad.RowingApp.Modeli.Vjezba;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VjezbaRepozitorij extends JpaRepository<Vjezba,Integer> {
}
