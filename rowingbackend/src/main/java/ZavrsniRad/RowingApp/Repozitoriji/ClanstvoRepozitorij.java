package ZavrsniRad.RowingApp.Repozitoriji;

import ZavrsniRad.RowingApp.Modeli.Clanstvo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClanstvoRepozitorij extends JpaRepository<Clanstvo,Clanstvo> {
}
