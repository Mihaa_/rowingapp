package ZavrsniRad.RowingApp.Modeli;

public class GrafPrikaz {
    private double ocekivanoVrijeme;
    private double rezultat;
    private int redniBroj;

    public GrafPrikaz(double ocekivanoVrijeme, double rezultat, int redniBroj) {
        this.ocekivanoVrijeme = ocekivanoVrijeme;
        this.rezultat = rezultat;
        this.redniBroj = redniBroj;
    }

    public double getOcekivanoVrijeme() {
        return ocekivanoVrijeme;
    }

    public void setOcekivanoVrijeme(double ocekivanoVrijeme) {
        this.ocekivanoVrijeme = ocekivanoVrijeme;
    }

    public double getRezultat() {
        return rezultat;
    }

    public void setRezultat(double rezultat) {
        this.rezultat = rezultat;
    }

    public int getRedniBroj() {
        return redniBroj;
    }

    public void setRedniBroj(int redniBroj) {
        this.redniBroj = redniBroj;
    }
}
