package ZavrsniRad.RowingApp.Modeli;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class Trening implements Comparable<Trening> {
    @Id
    @GeneratedValue
    private int id;

    @Column(nullable = false)
    private int idKorisnika;

    @Column(nullable = false)
    private int idVjezba;

    @Column(nullable = false)
    private java.util.Date datum;

    @Column(nullable = false)
    @ElementCollection
    private List<Double> rezultat;

    @Column(nullable = false)
    private double vrijemeTest;

    public Trening(){

    }

    public Trening(int idKorisnika, int idVjezba, Date datum, List<Double> rezultat, double vrijemeTest) {
        this.idKorisnika = idKorisnika;
        this.idVjezba = idVjezba;
        this.datum = datum;
        this.rezultat = rezultat;
        this.vrijemeTest = vrijemeTest;
    }

    public int getIdVjezba() {
        return idVjezba;
    }

    public void setIdVjezba(int idVjezba) {
        this.idVjezba = idVjezba;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdKorisnika() {
        return idKorisnika;
    }

    public void setIdKorisnika(int idKorisnika) {
        this.idKorisnika = idKorisnika;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public List<Double> getRezultat() {
        return rezultat;
    }

    public void setRezultat(List<Double> rezultat) {
        this.rezultat = rezultat;
    }

    public double getVrijemeTest() {
        return vrijemeTest;
    }

    public void setVrijemeTest(double vrijemeTest) {
        this.vrijemeTest = vrijemeTest;
    }


    @Override
    public int compareTo(Trening o) {
        return this.datum.compareTo(o.datum);
    }
}
