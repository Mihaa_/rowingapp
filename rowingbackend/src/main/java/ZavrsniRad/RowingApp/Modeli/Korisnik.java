package ZavrsniRad.RowingApp.Modeli;

import javax.persistence.*;

@Entity
@Table(name = "Korisnik")
public class Korisnik implements Comparable<Korisnik>{

    @Id
    @GeneratedValue
    private int id;

    @Column(nullable= false)
    private String lozinka;

    @Column(nullable = false)
    private String ime;

    @Column(nullable = false)
    private String prezime;

    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    private double vrijemeTest;

    public Korisnik(){

    }

    public Korisnik(String ime, String prezime, String email, double vrijemeTest) {

        this.ime = ime;
        this.prezime = prezime;
        this.email = email;
        this.vrijemeTest = vrijemeTest;
    }

    public String getLozinka() {
        return lozinka;
    }

    public void setLozinka(String lozinka) {
        this.lozinka = lozinka;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getVrijemeTest() {
        return vrijemeTest;
    }

    public void setVrijemeTest(double vrijemeTest) {
        this.vrijemeTest = vrijemeTest;
    }

    @Override
    public int compareTo(Korisnik o) {
        if(this.vrijemeTest>(o.vrijemeTest))
            return 1;
        if(this.vrijemeTest<o.vrijemeTest)
            return -1;
        else
            return 0;
    }
}
