package ZavrsniRad.RowingApp.Modeli;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.io.Serializable;

@Entity
@IdClass(Pozivnica.class)
public class Pozivnica implements Serializable {

    @Id
    @Column(nullable = false)
    private int idKorisnik;

    @Id
    @Column(nullable = false)
    private int idGrupa;

    public Pozivnica(){

    }

    public Pozivnica(int idKorisnik, int idGrupa) {
        this.idKorisnik = idKorisnik;
        this.idGrupa = idGrupa;
    }

    public int getIdKorisnik() {
        return idKorisnik;
    }

    public void setIdKorisnik(int idKorisnik) {
        this.idKorisnik = idKorisnik;
    }

    public int getIdGrupa() {
        return idGrupa;
    }

    public void setIdGrupa(int idGrupa) {
        this.idGrupa = idGrupa;
    }
}
