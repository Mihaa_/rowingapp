package ZavrsniRad.RowingApp.Modeli;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Grupa {

    @Id
    @GeneratedValue
    private int id;

    @Column(nullable = false)
    private String naziv;

    @Column(nullable = false)
    private boolean privatnost;

    @Column(nullable = false)
    private int idVlasnik;

    public Grupa(){
    }

    public Grupa(String naziv, boolean privatnost, int idVlasnik) {
        this.naziv = naziv;
        this.privatnost = privatnost;
        this.idVlasnik = idVlasnik;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public boolean isPrivatnost() {
        return privatnost;
    }

    public void setPrivatnost(boolean privatnost) {
        this.privatnost = privatnost;
    }

    public int getIdVlasnik() {
        return idVlasnik;
    }

    public void setIdVlasnik(int idVlasnik) {
        this.idVlasnik = idVlasnik;
    }
}
