package ZavrsniRad.RowingApp.Modeli;

import java.io.Serializable;

public class GrupPoz implements Serializable {

    private Grupa grupa;
    private Pozivnica pozivnica;

    public GrupPoz(Grupa grupa, Pozivnica pozivnica) {
        this.grupa = grupa;
        this.pozivnica = pozivnica;
    }

    public Grupa getGrupa() {
        return grupa;
    }

    public void setGrupa(Grupa grupa) {
        this.grupa = grupa;
    }

    public Pozivnica getPozivnica() {
        return pozivnica;
    }

    public void setPozivnica(Pozivnica pozivnica) {
        this.pozivnica = pozivnica;
    }
}
