package ZavrsniRad.RowingApp.Modeli;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;
import java.util.List;

public class TreningVjezba {
    private String naziv;
    private List<String> distance; //Distance mogu biti npr. 1000m,500m,1500m ili 20min, 30min
    private java.util.Date datum;
    private double vrijemeTest;
    private List<Double> rezultat;
    List<Double> ocekivanoVrijeme;
    private String napomena;

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public List<String> getDistance() {
        return distance;
    }

    public void setDistance(List<String> distance) {
        this.distance = distance;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public double getVrijemeTest() {
        return vrijemeTest;
    }

    public void setVrijemeTest(double vrijemeTest) {
        this.vrijemeTest = vrijemeTest;
    }

    public List<Double> getRezultat() {
        return rezultat;
    }

    public void setRezultat(List<Double> rezultat) {
        this.rezultat = rezultat;
    }

    public List<Double> getOcekivanoVrijeme() {
        return ocekivanoVrijeme;
    }

    public void setOcekivanoVrijeme(List<Double> ocekivanoVrijeme) {
        this.ocekivanoVrijeme = ocekivanoVrijeme;
    }

    public String getNapomena() {
        return napomena;
    }

    public void setNapomena(String napomena) {
        this.napomena = napomena;
		
    }

    public TreningVjezba(String naziv, List<String> distance, Date datum, double vrijemeTest, List<Double> rezultat, List<Double> ocekivanoVrijeme, String napomena) {
        this.naziv = naziv;
        this.distance = distance;
        this.datum = datum;
        this.vrijemeTest = vrijemeTest;
        this.rezultat = rezultat;
        this.ocekivanoVrijeme = ocekivanoVrijeme;
        this.napomena = napomena;
    }
}
