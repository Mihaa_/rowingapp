package ZavrsniRad.RowingApp.Modeli;

import javax.persistence.*;
import java.util.List;

@Entity
public class Vjezba {

    @Id
    @GeneratedValue
    private int id;

    @Column(nullable = false)
    private String naziv;

    @ElementCollection
    List<Double> ocekivanoVrijeme;

    private String napomena;

    @ElementCollection
    @Column(nullable = false)
    private List<String> distance; //Distance mogu biti npr. 1000m,500m,1500m ili 20min, 30min

    public Vjezba(){

    }

    public Vjezba(String naziv, List<Double> ocekivanoVrijeme, String napomena, List<String> distance) {
        this.naziv = naziv;
        this.ocekivanoVrijeme = ocekivanoVrijeme;
        this.napomena = napomena;
        this.distance = distance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public List<Double> getOcekivanoVrijeme() {
        return ocekivanoVrijeme;
    }

    public void setOcekivanoVrijeme(List<Double> ocekivanoVrijeme) {
        this.ocekivanoVrijeme = ocekivanoVrijeme;
    }

    public String getNapomena() {
        return napomena;
    }

    public void setNapomena(String napomena) {
        this.napomena = napomena;
    }

    public List<String> getDistance() {
        return distance;
    }

    public void setDistance(List<String> distance) {
        this.distance = distance;
    }
}
