package ZavrsniRad.RowingApp.Modeli;

import java.io.Serializable;

public class TrVjezba implements Serializable {
    private Trening trening;
    private Vjezba vjezba;

    public TrVjezba(Trening trening, Vjezba vjezba) {
        this.trening = trening;
        this.vjezba = vjezba;
    }

    public Trening getTrening() {
        return trening;
    }

    public void setTrening(Trening trening) {
        this.trening = trening;
    }

    public Vjezba getVjezba() {
        return vjezba;
    }

    public void setVjezba(Vjezba vjezba) {
        this.vjezba = vjezba;
    }
}
