import Dashboard from "views/Dashboard.jsx";
import UserProfile from "views/UserProfile.jsx";
import TableList from "views/TableList.jsx";
import Typography from "views/Typography.jsx";
import Icons from "views/Icons.jsx";
import Maps from "views/Maps.jsx";
import Notifications from "views/Notifications.jsx";
import Upgrade from "views/Upgrade.jsx";
import Login from "components/Login/Login";
import Registracija from "components/Registracija/Registracija";
import Vjezbe from "components/Vjezbe/Vjezbe";
import Treninzi from "components/Treninzi/Treninzi"
import Profil from "components/Profil/Profil";
import Logout from "components/Logout/Logout";
import Grupe from "components/Grupe/Grupe";
import MojePozivnice from "components/MojePozivnice/MojePozivnice";
import MojeGrupe from "components/MojeGrupe/MojeGrupe";
import StvoriGrupu from "components/StvoriGrupu/StvoriGrupu";

const dashboardRoutes = [
  {
    path: "/",
    name: "Login",
    icon: "",
    component: Login,
    layout: "/",
    invisible: true
  },
  {
    path: "/stvoriGrupu",
    name: "Login",
    icon: "",
    component: StvoriGrupu,
    layout: "/admin",
    invisible: true
  },
  {
    path: "/mojeGrupe",
    name: "Login",
    icon: "",
    component: MojeGrupe,
    layout: "/admin",
    invisible: true
  },
  {
    path: "/mojePozivnice",
    name: "Login",
    icon: "",
    component: MojePozivnice,
    layout: "/admin",
    invisible: true
  },
  {
    path: "/home",
    name: "Home",
    icon: "pe-7s-graph",
    component: Dashboard,
    layout: "/admin"
  },
  {
    path: "/profil",
    name: "Moj profil",
    icon: "pe-7s-user",
    component: Profil,
    layout: "/admin"
  },
  {
    path: "/treninzi",
    name: "Treninzi",
    icon: "pe-7s-note2",
    component: Treninzi,
    layout: "/admin"
  },
  {
    path: "/Grupe",
    name: "Grupe",
    icon: "pe-7s-news-paper",
    component: Grupe,
    layout: "/admin"
  },
  {
    path: "/vjezbe",
    name: "Vježbe",
    icon: "pe-7s-science",
    component: Vjezbe,
    layout: "/admin"
  },
  {
    path: "/registracija",
    name: "Registracija",
    icon: "pe-7s-science",
    component: Registracija,
    layout: "/admin",
    invisible:true
  },
  {
    path: "/logout",
    name: "LogOut",
    icon: "",
    component: Logout,
    layout: "/admin"
  },
];

export default dashboardRoutes;
