import React, { Component } from 'react';
import { Grid, Row, Col } from "react-bootstrap";
import { Card } from "components/Card/Card.jsx";
  import axios from "axios";
  import {Cookies} from 'react-cookie';
import { Button, FormGroup, FormControl, ControlLabel,Table } from "react-bootstrap";
import PopisVjezbi from 'components/PopisVjezbi/PopisVjezbi';
import {Redirect } from "react-router-dom";

export default class Vjezbe extends Component {

    state={
        napomena:"",
        naziv:"",
        distance:[""],
        ocekivanoVrijeme:[""],
        vjezba:{distance:[]}
    }

    constructor(props){
        super(props)
    }



    handleChange = event => {
        this.setState({
          [event.target.id]: event.target.value
        });
    }

    handleChangeDistance = (event, index) =>{
        this.state.distance[index]=event.target.value
        this.setState({distance:this.state.distance})
    }

    handleChangeOcekivano = (event,index)=>{
        this.state.ocekivanoVrijeme[index]=event.target.value
        this.setState({ocekivanoVrijeme:this.state.ocekivanoVrijeme})
    }

    handleSubmit = async event =>{

        const cookies = new Cookies()
          axios.defaults.headers.common['Authorization']='Bearer ' +cookies.get('token');

          await axios.post('https://rowingbackend.herokuapp.com/vjezba/dodaj',{
            "naziv": this.state.naziv,
            "ocekivanoVrijeme":this.state.ocekivanoVrijeme,
            "napomena": this.state.napomena,
            "distance": this.state.distance
        })

        this.setState({
            napomena:"",
            naziv:"",
            distance:[""],
            ocekivanoVrijeme:[""],
            vjezba:{distance:[]}
        })
        
    }

    handleDistanca = event =>{
        this.state.distance.push("")
        this.state.ocekivanoVrijeme.push("")
        this.setState(this.state)
    }

    prikazVjezbeHandler = (vjezba) =>{
        this.setState({vjezba:vjezba})
    }
    
    render() {
        var cookies=new Cookies()
        if(cookies.get("token")===undefined){
            return <Redirect to="/"></Redirect>
        }
        return (
            <div>
                <Grid fluid>
          
          <Row>
            <Col  md={4}>
              <Card
                statsIcon=""
                id="s"
                title=""
                category=""
                stats=""
                content={
                    <PopisVjezbi prikazVjezbeHandler={this.prikazVjezbeHandler} />
                }
              />
            </Col>
            <Col md={8}>
            <Card
                title={this.state.vjezba.naziv}
                category=""
                stats=""
                content={
                    <div>
                        <Table striped hover>
                        <thead>
                        <tr>
                            <th>Distance</th>
                            <th>Očekivano vrijeme</th>
                        </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.vjezba.distance.map((dist,index)=>{
                                    return <tr>
                                        <td>{dist}</td>
                                        <td>+{this.state.vjezba.ocekivanoVrijeme[index]}</td>
                                    </tr>
                                })                            
                            }               
                        </tbody>
                        </Table>
                        <hr/>
                        <b>Napomena: </b>{this.state.vjezba.napomena}
                    </div>
                }
            />
            <Card
                title="Dodaj novu vježbu"
                category=""
                stats=""
                content={<div>
                    <FormGroup controlId="naziv" bsSize="large">
                        <ControlLabel>Naziv</ControlLabel>
                        <FormControl
                        value={this.state.naziv}
                        onChange={this.handleChange}
                        type="texts"
                        />
                    </FormGroup>
                    <FormGroup controlId="napomena" bsSize="large">
                        <ControlLabel>Napomena</ControlLabel>
                        <FormControl
                        value={this.state.napomena}
                        onChange={this.handleChange}
                        type="texts"
                        />
                     </FormGroup>

                    {this.state.distance.map((distance,index)=>{
                        return (
                            <Row>
                            <Col md={6}>
                                <FormGroup controlId="" bsSize="medium">
                                    <ControlLabel>Distance</ControlLabel>
                                    <FormControl
                                    value={this.state.distance[index]}
                                    onChange={(event)=>this.handleChangeDistance(event,index)}
                                    type="texts"
                                    />
                                </FormGroup>
                            </Col>
                            <Col md={6}>
                                <FormGroup controlId={this.state.ocekivanoVrijeme[index]} bsSize="medium">
                                    <ControlLabel>Ocekivano vrijeme</ControlLabel>
                                    <FormControl
                                    value={this.state.ocekivanoVrijeme[index]}
                                    onChange={(event)=>this.handleChangeOcekivano(event,index)}
                                    type="texts"
                                    />
                                </FormGroup>
                            </Col>
                        </Row>
                        );
                    })}
                    <div align="center">
                        <Button onClick={this.handleDistanca}>+Distanca</Button>
                    </div>
                    </div>    
                }
                legend={
                    <Button bsStyle="info" pullRight fill type="submit" onClick={this.handleSubmit}>
                      Dodaj vježbu
                    </Button>
                }
            />
            </Col>
          </Row>

         
        </Grid>
            </div>
        )
    }
}
