import React, { Component } from 'react';
import { Grid, Row, Col } from "react-bootstrap";
import { Card } from "components/Card/Card.jsx";
import axios from "axios";
import {Cookies} from 'react-cookie';
import { Button} from "react-bootstrap";
import {Redirect } from "react-router-dom";
import Select from 'react-select';

export default class MojeGrupe extends Component {

    state={
        idGrupa:"",
        idKorisnik:"",
        sviKorisnici:[],
        sveGrupe:[],
        poruka:""
    }

    

   async componentDidMount(){
        var pomKorisnici=[]
        var pomGrupe=[]
        const cookies= new Cookies;      
        axios.defaults.headers.common['Authorization']='Bearer ' +cookies.get('token');

        await axios.get("https://rowingbackend.herokuapp.com/grupa/mojeGrupe/"+cookies.get("id")).then(resp=>{
            resp.data.map(grupa=>{
                pomGrupe.push({
                    label:grupa.naziv,
                    value:grupa.id
                })
            })
        })
        this.setState({sveGrupe:pomGrupe})

       await axios.get("https://rowingbackend.herokuapp.com/korisnik/dohvati").then(resp=>{
            resp.data.map(kor=>{
                pomKorisnici.push({
                    label:kor.ime +" "+kor.prezime,
                    value:kor.id
                })
            })
        })
        this.setState({
            sviKorisnici:pomKorisnici
        })
    }

    odabirGrupe= (event)=>{
        this.setState({idGrupa:event.value})
    }

    odabirKorisnika = (event)=>{
        this.setState({idKorisnik:event.value})
    }

    posaljiPozivnicu=async ()=>{
        const cookies= new Cookies; 
        await axios.post("https://rowingbackend.herokuapp.com/pozivnica/dodaj/"+cookies.get("id"),{
            
                "idKorisnik": this.state.idKorisnik,
                "idGrupa": this.state.idGrupa
            
        })

        this.setState({
            poruka:"Pozivnica poslana korisniku "+this.state.idKorisnik
        })
    }

    

    render() {
        var cookies=new Cookies()
        if(cookies.get("token")===undefined){
            return <Redirect to="/"></Redirect>
        }
        return (
            <div>
                <Col md={2}/>
                <Col md={8}>
                <Card 
                    title="Pošalji pozivnice"
                    content={
                        <div>
                            <div style={{
                                                    paddingTop: 20
                                                }}>
                            <Select placeholder="Odaberi grupu" onChange={this.odabirGrupe} options={ this.state.sveGrupe } />
                            </div>
                            <div style={{
                                                    paddingTop: 20,
                                                    paddingBottom:20
                                                }}>
                            <Select placeholder="Odaberi Korisnika" onChange={this.odabirKorisnika} options={ this.state.sviKorisnici} />
                            </div>
                            <Button bsStyle="info" pullRight fill type="submit" onClick={this.posaljiPozivnicu}>
                          Pošalji pozivnicu
                         </Button>
                         <p>{this.state.poruka}</p>
                        </div>
                    }
                />
                </Col>
            </div>
        )
    }
}
