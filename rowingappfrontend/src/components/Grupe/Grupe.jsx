import React, { Component } from 'react';
import { Grid, Row, Col } from "react-bootstrap";
import { Card } from "components/Card/Card.jsx";
import axios from "axios";
import {Cookies} from 'react-cookie';
import { Button,Table } from "react-bootstrap";
import {Redirect } from "react-router-dom";
import Select from 'react-select';


export default class Grupe extends Component {

    state={
        grupaZaPrikaz:"",
        clanovi:[""],
        popisSvihGrupa:[]
    }

    

    selectGrupaHandler = (event)=>{
        this.setState({
            grupaZaPrikaz:event.value
        })
        axios.get("https://rowingbackend.herokuapp.com/clanstvo/dohvatiClanove/"+event.value.id).then(resp=>{
            this.setState({clanovi:resp.data})
        })
    }

    componentDidMount(){
        const cookies= new Cookies;    
        var pomGrupe=[]  
        axios.defaults.headers.common['Authorization']='Bearer ' +cookies.get('token');
        axios.get("https://rowingbackend.herokuapp.com/grupa/uclanjenjeGrupe/"+cookies.get("id")).then(resp=>{
            resp.data.map(grupa=>{
                pomGrupe.push(
                    {
                        label:grupa.naziv,
                        value:grupa
                    }
                )
            })
            this.setState({popisSvihGrupa:pomGrupe})
        })
    }

    handleMojeGrupe= ()=>{
        console.log("Moje grupe!")
        this.props.history.push({
            pathname: '/admin/mojeGrupe'
        });
    }

    handleStvoriGrupu = ()=>{
        console.log("Stvori Grupu")
        this.props.history.push({
            pathname: '/admin/stvoriGrupu'
        });
    }

    handleMojePozivnice = ()=>{
        console.log("Moje pozivnice")
        this.props.history.push({
            pathname: '/admin/mojePozivnice'
        });
    }

    render() {
        var cookies=new Cookies()
        if(cookies.get("token")===undefined){
            return <Redirect to="/"></Redirect>
        }
        return (
            <div>
                <Grid fluid>
                    
                    <Row>
                        <Col>
                            <Card
                            title=""
                            content={
                                <div>
                                    <Grid>
                                        <div style={{
                                            paddingBottom: 30
                                          }}>
                                        <Row>
                                            <Col md={2}/>
                                            <Col md={3}>
                                                <Button bsStyle="info" pullRight fill type="submit" onClick={this.handleStvoriGrupu}>
                                                Stvori grupu
                                                </Button>
                                            </Col>
                                            <Col md ={3}>
                                                <Button bsStyle="info" pullRight fill type="submit" onClick={this.handleMojeGrupe}>
                                                Moje grupe
                                                </Button>
                                            </Col>
                                            <Col md ={3}>
                                                <Button bsStyle="info" pullRight fill type="submit" onClick={this.handleMojePozivnice}>
                                                Moje pozivnice
                                                </Button>
                                            </Col>
                                        </Row>
                                        </div>
                                        <Row>
                                            <Select placeholder="Odaberi grupu" onChange={this.selectGrupaHandler} options={ this.state.popisSvihGrupa } />
                                        </Row>
                                        <Row>
                                        <Table striped hover>
                                            <thead>
                                            <tr>
                                                <th>Rang</th>
                                                <th>Ime</th>
                                                <th>Prezime</th>
                                                <th>Vrijeme test</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.clanovi.map((clan,index)=>{
                                                    var rezPom=clan.vrijemeTest
                                                     var rez=Math.floor(rezPom/60)+ (rezPom%60)/100
                                                     rez=Number((rez).toFixed(3))
                                                    return <tr>
                                                        <th>{index +1}</th>
                                                        <th>{clan.ime}</th>
                                                        <th>{clan.prezime}</th>
                                                        <th>{rez}</th>
                                                    </tr>
                                                })  }   
                                            </tbody>
                                        </Table>
                                        </Row>
                                    </Grid>
                                </div>
                            }
                             />
                        </Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}
