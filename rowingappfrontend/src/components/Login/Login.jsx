import React, { Component } from "react";
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import "./Login.css";
import Axios from 'axios';
import {Cookies} from 'react-cookie';

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
      token:'',
      id:""
    };
  }



  validateForm() {
    return this.state.email.length > 0 && this.state.password.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleSubmit =async event => {
    event.preventDefault();
    await Axios.post('https://rowingbackend.herokuapp.com/login',{
        "email": this.state.email,
        "lozinka": this.state.password
    }).then(resp =>{ this.setState({token:resp.data.token})})

    Axios.defaults.headers.common['Authorization']='Bearer ' +this.state.token;
    await Axios.get("https://rowingbackend.herokuapp.com/korisnik/dohvatiId/"+this.state.email+".").then(resp =>{
      this.setState({id:resp.data})
    })


    const cookies = new Cookies();
    cookies.set('token', this.state.token, { path: '/' });
    cookies.set('email', this.state.email, { path: '/' });
    cookies.set('id', this.state.id, { path: '/' });

    this.setState({})

    this.props.history.push({
      pathname: '/admin/home'
  });
    
  
    
  }

  render() {
    return (
      <div className="Login">
        <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="email" bsSize="large">
            <ControlLabel>Email</ControlLabel>
            <FormControl
              autoFocus
              type="email"
              value={this.state.email}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup controlId="password" bsSize="large">
            <ControlLabel>Password</ControlLabel>
            <FormControl
              value={this.state.password}
              onChange={this.handleChange}
              type="password"
            />
          </FormGroup>
          <Button
            block
            bsSize="large"
            disabled={!this.validateForm()}
            type="submit"
          >
            Login
          </Button>
          <p>Nemate korisnički račun? <a href="/admin/registracija">Registriraj se!</a></p>
        </form>
      </div>
    );
  }
}