import React, { Component } from 'react'
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import "./Registracija.css";
import Axios from 'axios';

export class Registracija extends Component {

    constructor(props) {
        super(props);
    
        this.state = {
          email: "",
          password: "",
          ime:"",
          prezime:"",
          vrijemeTest:""
        };
      }

      
    validateForm() {
        return this.state.email.length > 0 && this.state.password.length > 0;
    }

    handleChange = event => {
        this.setState({
          [event.target.id]: event.target.value
        });
      }

      handleSubmit = event => {
        var rez=this.state.vrijemeTest
        rez=parseFloat(Math.floor(rez)*60) + parseFloat(Number((rez%1)*100).toFixed(2))
        event.preventDefault();
        Axios.post('https://rowingbackend.herokuapp.com/users/sign-up',{

                email: this.state.email,
                lozinka: this.state.password,
                ime:this.state.ime,
                prezime:this.state.prezime,
                vrijemeTest:rez
              
        })
        
        
            console.log("Korisnik registriran");
            this.props.history.push({
                pathname: '/'
            });
    
      }
    render() {
        return (
            <div className="Login">
        <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="email" bsSize="large">
            <ControlLabel>Email</ControlLabel>
            <FormControl
              autoFocus
              type="email"
              value={this.state.email}
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup controlId="password" bsSize="large">
            <ControlLabel>Password</ControlLabel>
            <FormControl
              value={this.state.password}
              onChange={this.handleChange}
              type="password"
            />
          </FormGroup>
          <FormGroup controlId="ime" bsSize="large">
            <ControlLabel>Ime</ControlLabel>
            <FormControl
              value={this.state.ime}
              onChange={this.handleChange}
              type=""
            />
          </FormGroup>
          <FormGroup controlId="prezime" bsSize="large">
            <ControlLabel>Prezime</ControlLabel>
            <FormControl
              value={this.state.prezime}
              onChange={this.handleChange}
              type=""
            />
          </FormGroup>
          <FormGroup controlId="vrijemeTest" bsSize="large">
            <ControlLabel>Vrijeme test</ControlLabel>
            <FormControl
              value={this.state.vrijemeTest}
              onChange={this.handleChange}
              type=""
              placeholder="1.452"
            />
          </FormGroup>
          <Button
            block
            bsSize="large"
            disabled={!this.validateForm()}
            type="submit"
          >
            Registriraj se
          </Button>
        </form>
      </div>
        )
    }
}

export default Registracija
