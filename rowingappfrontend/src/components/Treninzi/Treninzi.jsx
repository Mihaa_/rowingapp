import React, { Component } from 'react';
import { Grid, Row, Col } from "react-bootstrap";
import { Card } from "components/Card/Card.jsx";
import {
    responsiveSales,
    legendSales
  } from "variables/Variables.jsx";
import PopisTreninga from "components/PopisTreninga/PopisTreninga";
import {Cookies} from 'react-cookie';
import { Table } from "react-bootstrap";
import ChartistGraph from "react-chartist";
import TreningForma from "components/TreningForma/TreningForma";
import {Redirect } from "react-router-dom";

export default class Treninzi extends Component {

    state={
        trening:{
            vjezba:{
                distance:[""],
                ocekivanoVrijeme:[""]
            },
            trening:{
                rezultat:[""]
            }
        },
        dataSales : {
            labels: [
              "1.5",
              "2.5",
              "3.5",
              "4.5",
              "5.5",
            ],
            series: [
              [1.45,1.43,1.40,1.39,1.38],
              [1.45,1.40,1.38,1.42,1.43],
            ]
          },
          optionsSales:{
            low: 1.35,
            high: 1.45,
            showArea: false,
            height: "300px",
            axisX: {
              showGrid: false
            },
            lineSmooth: true,
            showLine: true,
            showPoint: true,
            fullWidth: true,
            chartPadding: {
              right: 50
            }
          }
    }

    grafHandler=(tren)=>{
        var pom={
            rezultati:[],
            ocekivanoVrijeme:[],
            labels:[]
          }
      
          var donjaGranica=10;
          var gornjaGranica=0;
      
          for(var i=0; i<tren.trening.rezultat.length;i++){
            var rez=Math.floor(tren.trening.rezultat[i]/60)+ (tren.trening.rezultat[i]%60)/100
            pom.rezultati.push(rez)
          }
      
          for(var i=0;i < tren.vjezba.ocekivanoVrijeme.length;i++){
            var vrijeme=tren.vjezba.ocekivanoVrijeme[i]+tren.trening.vrijemeTest
            pom.ocekivanoVrijeme.push(Math.floor(vrijeme/60)+(vrijeme%60)/100)
            pom.labels.push(tren.vjezba.distance[i])
          }
          console.log(tren.vjezba.ocekivanoVrijeme)
          
          this.setState({trening:tren})
          
          this.setState({dataSales : {
            labels: pom.labels,
            series: [
              pom.rezultati,
              pom.ocekivanoVrijeme,
            ],
          }})

          pom.rezultati.map(rez=>{
            if(rez>gornjaGranica){
              gornjaGranica=rez;
            }
            if(rez<donjaGranica){
              donjaGranica=rez
            }
          })

          pom.ocekivanoVrijeme.map(rez=>{
            if(rez>gornjaGranica){
              gornjaGranica=rez;
            }
            if(rez<donjaGranica){
              donjaGranica=rez
            }
          })
          console.log("Gornja: "+(parseFloat(gornjaGranica)+0.08))
          console.log("Donja: "+ (parseFloat(donjaGranica)-0.08))

          this.setState({
            optionsSales:{
              low: donjaGranica,
              high: gornjaGranica,
              showArea: false,
              height: "300px",
              axisX: {
                showGrid: false
              },
              lineSmooth: true,
              showLine: true,
              showPoint: true,
              fullWidth: true,
              chartPadding: {
                right: 50
              }
            }
          })
    }

    createLegend(json) {
        var legend = [];
        for (var i = 0; i < json["names"].length; i++) {
          var type = "fa fa-circle text-" + json["types"][i];
          legend.push(<i className={type} key={i} />);
          legend.push(" ");
          legend.push(json["names"][i]);
        }
        return legend;
      }

    render() {
      var cookies=new Cookies()
        if(cookies.get("token")===undefined){
            return <Redirect to="/"></Redirect>
        }
        return (
            <div>
                <Grid fluid>
          
          <Row>
            <Col  md={4}>
              <Card
                statsIcon=""
                id="s"
                title=""
                category=""
                stats=""
                content={
                    <PopisTreninga grafHandler={this.grafHandler}/>
                }
              />
            </Col>
            <Col md={8}>
            <Card
                title={this.state.trening.vjezba.naziv +" "+ new Intl.DateTimeFormat('hr-HR', { 
                    year: 'numeric', 
                    month: 'long', 
                    day: '2-digit' 
                  }).format(this.state.trening.trening.datum)}
                category=""
                stats=""
                content={
                    <div>
                    <ChartistGraph
                    data={this.state.dataSales}
                    type="Line"
                    options={this.state.optionsSales}
                    responsiveOptions={responsiveSales}
                  />
                  </div>
                }
                legend={
                    <div className="legend">{this.createLegend(legendSales)}</div>
                  }
            />
            <Card
                title=""
                category=""
                stats=""
                content={  
                    <div>
                        <Table striped hover>
                        <thead>
                        <tr>
                            <th>Distance</th>
                            <th>Očekivano vrijeme</th>
                            <th>rezultat</th>
                        </tr>
                        </thead>
                        <tbody>
                            {this.state.trening.vjezba.distance.map((dist,index)=>{
                                if(dist===""){
                                    return(<tr></tr>)
                                }
                                var rezPom=this.state.trening.trening.rezultat[index]
                                var rez=Math.floor(rezPom/60)+ (rezPom%60)/100
                                rez=Number((rez).toFixed(3))

                                var ocekPom=this.state.trening.vjezba.ocekivanoVrijeme[index]+this.state.trening.trening.vrijemeTest
                                var ocek=Math.floor(ocekPom/60)+ (ocekPom%60)/100
                                
                                ocek=Number((ocek).toFixed(3))
                                var style= (rez<=ocek) ? "bg-success":"bg-danger"
                                var razlika=Number(((rez-ocek)*100).toFixed(2))
                                return (
                                    <tr>
                                        <td>{dist}</td>
                                        <td>{ocek}   (+{this.state.trening.vjezba.ocekivanoVrijeme[index]})</td>
                                        <td><p className={style}>{rez}   ({razlika>0?"+":""}{razlika})</p></td>
                                    </tr>
                                )
                            })
                            }            
                        </tbody>
                        </Table>
                    </div> 
                }
            />
            <TreningForma/>
            </Col>
          </Row>

         
        </Grid>
            </div>
        )
    }
}
