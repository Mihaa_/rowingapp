import React, { Component } from 'react';
import { Grid, Row, Col } from "react-bootstrap";
import { Card } from "components/Card/Card.jsx";
  import axios from "axios";
  import {Cookies} from 'react-cookie';
import { Button} from "react-bootstrap";
import {Redirect } from "react-router-dom";

export class MojePozivnice extends Component {

    state={
        pozivnice:[]
    }

   async componentDidMount(){
        const cookies= new Cookies;      
        axios.defaults.headers.common['Authorization']='Bearer ' +cookies.get('token');
        var pomGrupe=[]

        await axios.get("https://rowingbackend.herokuapp.com/pozivnica/dohvati/MojePozivnice/"+cookies.get("id")).then(resp=>{
            this.setState({
                pozivnice:resp.data
            })
        })
    }

    handlePrihvati=(pozivnica)=>{
        const cookies= new Cookies;
        axios.post("https://rowingbackend.herokuapp.com/pozivnica/prihvati/"+cookies.get("id"),pozivnica)
        console.log("Prihvacam")
        console.log(pozivnica)
    }

    handleOdbij=(pozivnica)=>{
        axios.post("https://rowingbackend.herokuapp.com/pozivnica/odbij",pozivnica)
        console.log("Odbijam")
        console.log(pozivnica)
    }
    render() {
        var cookies=new Cookies()
        if(cookies.get("token")===undefined){
            return <Redirect to="/"></Redirect>
        }
        return (
            <div>
                <Col md={2}/>
                <Col md={8}>
                <Card
                    title="Pozivnice"
                    content={
                        this.state.pozivnice.map(poz=>{
                            return <Row> <Col md={9}><button type="button" className="list-group-item list-group-item">{poz.grupa.naziv}</button></Col>
                                           <Col md={1}> <Button onClick={()=>{this.handlePrihvati(poz.pozivnica)}}>Prihvati</Button></Col>
                                           <Col md={1}><Button onClick={()=>{this.handleOdbij(poz.pozivnica)}}>Odbij</Button></Col>
                                    </Row>
                        })
                        
                    }
                />
                </Col>
            </div>
        )
    }
}

export default MojePozivnice
