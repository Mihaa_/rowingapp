import React, { Component } from 'react';
import {Redirect } from "react-router-dom";
import {Cookies} from 'react-cookie';


export default class Logout extends Component {

    componentDidMount(){
        const cookies=new Cookies()
        cookies.remove('id', { path: '/' })
        cookies.remove('email', { path: '/' })
        cookies.remove('token', { path: '/' })
    }
    render() {
        return (
           <Redirect to="/"/>
        )
    }
}
