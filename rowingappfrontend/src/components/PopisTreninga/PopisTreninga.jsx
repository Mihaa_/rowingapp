import React, { Component } from 'react'
import axios from 'axios';
import {Cookies} from 'react-cookie';

export class PopisTreninga extends Component {

    state={
        treninzi:[
            
        ]
    }

    async componentDidMount(){
        const cookies= new Cookies()

        axios.defaults.headers.common['Authorization']='Bearer ' +cookies.get('token');
        await axios.get('https://rowingbackend.herokuapp.com/trening/dohvati/mojiTreninzi/'+cookies.get("id")).then(resp=>{
            this.setState({treninzi:resp.data})
        })
    }

    render() {
        try{
            return (
                <div className="list-group">
                    
                        {
                            this.state.treninzi.map(tren=>{
                               return <button onClick={()=>this.props.grafHandler(tren)} type="button" className="list-group-item list-group-item">{tren.vjezba.naziv +' '+ new Intl.DateTimeFormat('hr-HR', { 
                                year: 'numeric', 
                                month: 'long', 
                                day: '2-digit' 
                              }).format(tren.trening.datum)}</button>
                                
                            })
                        }
                    
                </div>
            )
        }catch(exc){
            return null;
        }
        
        
    }
}

export default PopisTreninga
