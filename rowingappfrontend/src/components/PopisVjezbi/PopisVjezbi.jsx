import React, { Component } from 'react';

import axios from 'axios';
import {Cookies} from 'react-cookie';

export default class PopisVjezbi extends Component {

    state={
        vjezbe:[]
    }

    constructor(props){
        super(props)
    }

   async componentDidMount(){

        
        const cookies= new Cookies;
        await this.setState({
            email:cookies.get('email'),
            token:cookies.get('token')
          })
      
        axios.defaults.headers.common['Authorization']='Bearer ' +this.state.token;
        
        await axios.get('https://rowingbackend.herokuapp.com/vjezba/dohvati').then(resp=>{
        this.setState({vjezbe:resp.data})
        })
    }

    render() {
        try{
            return (
                <div className="list-group">
                    
                        {
                            this.state.vjezbe.map(vjezba=>{
                               return <button onClick={()=>this.props.prikazVjezbeHandler(vjezba)} type="button" class="list-group-item list-group-item">{vjezba.naziv}</button>
                            })
                        }
                    
                </div>
            )
        }catch(exc){
            return null;
        }
        
        
    }
}
