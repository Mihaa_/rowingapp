import React, { Component } from 'react';
import { Col } from "react-bootstrap";
import { Card } from "components/Card/Card.jsx";
import axios from "axios";
import {Cookies} from 'react-cookie';
import { Button, FormGroup, FormControl, ControlLabel,Table } from "react-bootstrap";
import {Redirect } from "react-router-dom";

export default class StvoriGrupu extends Component {

    state={
        naziv:"",
        privatnost:false,
        idVlasnika:""
    }

    stvoriGrupuHandler =async ()=>{
        const cookies= new Cookies;      
        axios.defaults.headers.common['Authorization']='Bearer ' +cookies.get('token');

        await axios.post('https://rowingbackend.herokuapp.com/grupa/dodaj',{
            "naziv": this.state.naziv,
            "privatnost": false,
            "idVlasnik": cookies.get("id")
        })

        this.setState({naziv:""})
    }

    handleNazivGrupe = (event) =>{
        this.setState({naziv:event.target.value})
    }

    render() {
        var cookies=new Cookies()
        if(cookies.get("token")===undefined){
            return <Redirect to="/"></Redirect>
        }
        return (
            <div>
                <Col md={2}/>
                <Col md={8}>
                <Card
                title="Stvori novu grupu"
                content={
                    <div>
                        <Col>
                        <FormGroup controlId="" bsSize="medium">
                                    <ControlLabel>Naziv grupe</ControlLabel>
                                    <FormControl
                                    value={this.state.naziv}
                                    onChange={(event)=>this.handleNazivGrupe(event)}
                                    type="texts"
                                    />
                                </FormGroup>
                        </Col>
                        <Col>
                        <Button bsStyle="info" pullRight fill type="submit" onClick={this.stvoriGrupuHandler}>
                          Kreiraj grupu
                         </Button>
                        </Col>
                    </div>
                } />
                </Col>
            </div>
        )
    }
}
