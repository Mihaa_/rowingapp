import React, { Component } from 'react';
import {
    Button,
    Grid,
    Row,
    Col,
    FormGroup,
    ControlLabel,
    FormControl
  } from "react-bootstrap";
import { Card } from "components/Card/Card.jsx";
import axios from "axios";
import {Cookies} from 'react-cookie';
import {Redirect } from "react-router-dom";


export default class Profil extends Component {

    state={
            ime:"",
            prezime:"",
            email:"",
            vrijemeTest:""
        
    }

   async componentDidMount(){
        const cookies= new Cookies;      
        axios.defaults.headers.common['Authorization']='Bearer ' +cookies.get('token');

        await axios.get("https://rowingbackend.herokuapp.com/korisnik/dohvati/"+cookies.get("id")).then(resp =>{
            var rezPom=resp.data.vrijemeTest
            var rez=Math.floor(rezPom/60)+ (rezPom%60)/100
            rez=Number((rez).toFixed(3))
            this.setState({
                    ime:resp.data.ime,
                    prezime:resp.data.prezime,
                    email:resp.data.email,
                    vrijemeTest:rez
                
            })
        })
    }

    azurirajVrijemeTest =async ()=>{
        console.log("Azurirao si vrijeme")
        console.log(this.state.vrijemeTest)
        var rez=this.state.vrijemeTest
        rez=parseFloat(Math.floor(rez)*60) + parseFloat(Number((rez%1)*100).toFixed(2))

        const cookies= new Cookies;  
        await axios.post("https://rowingbackend.herokuapp.com/korisnik/azuriraj",{
            
                "id": cookies.get("id"),
                "vrijemeTest": rez
            
        })
    }
    
    handleVrijemeTest = (event) =>{
        this.setState({vrijemeTest:event.target.value})
    }


    render() {
        var cookies=new Cookies()
        if(cookies.get("token")===undefined){
            return <Redirect to="/"></Redirect>
        }
        return (
            <div>
                <Grid fluid>
                    <Row>
                        <Col md={2}></Col>
                        <Col md={8}>
                            <Card
                                title="Profil"
                                content={
                                    <Grid fluid>
                                        <Row>
                                            <Col md={1}/>
                                            <Col md={5}>
                                                <FormGroup controlId="" bsSize="medium">
                                                    <ControlLabel>Ime</ControlLabel>
                                                    <FormControl
                                                        value={this.state.ime}
                                                        type="texts"
                                                        disabled
                                                    />  
                                                </FormGroup>
                                            </Col>
                                            <Col md={5}>
                                                 <FormGroup controlId="" bsSize="medium">
                                                    <ControlLabel>Prezime</ControlLabel>
                                                    <FormControl
                                                        value={this.state.prezime}
                                                        type="texts"
                                                        disabled
                                                    />  
                                                </FormGroup>
                                            </Col>
                                            <Col md={1}/>
                                        </Row>
                                        <Row>
                                        <Col md={1}/>
                                            <Col md={10}>
                                          <FormGroup controlId="" bsSize="medium">
                                                <ControlLabel>Email</ControlLabel>
                                                <FormControl
                                                    value={this.state.email}
                                                    type="texts"
                                                    disabled
                                                />  
                                             </FormGroup>
                                             </Col>
                                             <Col md={1}/>
                                        </Row>
                                        <Row>
                                            <Col md={1}/>
                                            <Col md={5}>
                                            <FormGroup controlId="" bsSize="medium">
                                                    <ControlLabel>Vrijeme test</ControlLabel>
                                                    <FormControl
                                                        value={this.state.vrijemeTest}
                                                        onChange={this.handleVrijemeTest}
                                                        type="texts"
                                                    />  
                                                </FormGroup>
                                            </Col>
                                            <Col md={2}/>
                                            <Col md={2}>
                                                <div style={{
                                                    paddingTop: 20
                                                }}>
                                                    <Button bsStyle="info" pullRight fill type="submit" onClick={this.azurirajVrijemeTest}>
                                                    Update
                                                </Button>
                                                </div>   
                                            </Col>
                                            <Col md={1}/>
                                        </Row>
                                    </Grid>
                                }/>
                        </Col>
                        <Col md={2}></Col>
                    </Row>
                </Grid>
            </div>
        )
    }
}


