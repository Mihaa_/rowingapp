import React, { Component } from 'react';
import { Grid, Row, Col } from "react-bootstrap";
import { Card } from "components/Card/Card.jsx";
import axios from "axios";import {Cookies} from 'react-cookie';
import {Button, FormGroup, FormControl, ControlLabel,Table } from "react-bootstrap";
import Select from 'react-select';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";


export default class TreningForma extends Component {

    state={
        vrijemeTest:"",
        idKorisnika:"",
        datum:new Date(),
        rezultati:[],
        distance:[],
        idVjezba:"",
        vjezbe:[]
    }

  

    componentDidMount(){
        const cookies= new Cookies;      
        axios.defaults.headers.common['Authorization']='Bearer ' +cookies.get('token');

        axios.get("https://rowingbackend.herokuapp.com/korisnik/dohvati/"+cookies.get("id")).then(resp =>{
            this.setState({
                vrijemeTest:resp.data.vrijemeTest,
                idKorisnika:cookies.get("id")
            })
        })

        axios.get('https://rowingbackend.herokuapp.com/vjezba/dohvati').then(resp=>{
        this.setState({vjezbe:resp.data})
        })
    }

    selectVjezbaHandler =async (event)=>{
        await this.setState({
            idVjezba:event.value
        })
        axios.get('https://rowingbackend.herokuapp.com/vjezba/dohvati/'+this.state.idVjezba).then(resp=>{
        this.setState({distance:resp.data.distance})
        })
    }

    handleChangeRezultati = (event, index) =>{
        this.state.rezultati[index]=event.target.value
        this.setState({rezultati:this.state.rezultati})
    }

    handleDatumChange = (date)=> {
        this.setState({
          datum: date
        });
      }

      handleDodajTrening =async()=>{
          console.log("Dodao si trening")
          var pomRezultati=[]
         await this.state.rezultati.map((rez,index)=>{

            rez=parseFloat(Math.floor(rez)*60) + parseFloat(Number((rez%1)*100).toFixed(2))
              pomRezultati[index]=rez
          })

         await axios.post('https://rowingbackend.herokuapp.com/trening/dodaj',{
            "idKorisnika": this.state.idKorisnika,
            "idVjezba": this.state.idVjezba,
            "datum": this.state.datum,
            "rezultat": pomRezultati,
            "vrijemeTest": this.state.vrijemeTest
        }) 

        this.setState({rezultati:[],
        distance:[]})
      }


    render() {

          const techCompaniesA=[]
          this.state.vjezbe.map(vjezba=>{
              techCompaniesA.push({
                  label:vjezba.naziv,
                  value:vjezba.id
              })
          })
        return (
            <div>
                <Card
                statsIcon=""
                id="s"
                title="Unos treninga"
                category=""
                stats=""
                content={
                    <div>
                    <Row >
                    <Col md={6}>
                     <Select placeholder="Odaberi vježbu" onChange={this.selectVjezbaHandler} options={ techCompaniesA } />
                    </Col>
                    <Col md={4}>
                        <div>
                       <DatePicker
                            inline
                            selected={this.state.datum}
                            onChange={this.handleDatumChange}
                        />
                        </div>
                    </Col>
                    </Row>
                    {this.state.distance.map((distance,index)=>{
                        return (
                            <Row>
                            <Col md={6}>
                                <FormGroup controlId="" bsSize="medium">
                                    <ControlLabel>{distance}</ControlLabel>
                                    <FormControl
                                    value={this.state.rezultati[index]}
                                    onChange={(event)=>this.handleChangeRezultati(event,index)}
                                    type="texts"
                                    />
                                </FormGroup>
                            </Col>
                        </Row>
                        );
                    })}
                   

                    </div>}
                     legend={
                        <Button bsStyle="info" pullRight fill type="submit" onClick={this.handleDodajTrening}>
                          Dodaj trening
                        </Button>
                    }
              />
            </div>
        )
    }
}
