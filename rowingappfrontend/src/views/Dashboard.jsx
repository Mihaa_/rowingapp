import React, { Component } from "react";
import ChartistGraph from "react-chartist";
import { Grid, Row, Col } from "react-bootstrap";
import { Card } from "components/Card/Card.jsx";
import { StatsCard } from "components/StatsCard/StatsCard.jsx";
import { Tasks } from "components/Tasks/Tasks.jsx";
import {
  dataPie,
  legendPie,
  optionsSales,
  responsiveSales,
  legendSales,
  dataBar,
  optionsBar,
  responsiveBar,
  legendBar
} from "variables/Variables.jsx";
import axios from "axios";
import PopisTreninga from "components/PopisTreninga/PopisTreninga";
import {Cookies} from 'react-cookie';
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

class Dashboard extends Component {


  state={
    email:'',
    token:'',
    idKorisnika: 0,
    dataSales : {
      labels: [
        "1.5",
        "2.5",
        "3.5",
        "4.5",
        "5.5",
      ],
      series: [
        [1.45,1.43,1.40,1.39,1.38],
        [1.45,1.40,1.38,1.42,1.43],
      ]
    },
    optionsSales:{
      low: 1.35,
      high: 1.45,
      showArea: false,
      height: "300px",
      axisX: {
        showGrid: false
      },
      lineSmooth: true,
      showLine: true,
      showPoint: true,
      fullWidth: true,
      chartPadding: {
        right: 50
      }
    }
  }

  constructor(props) {
    super(props);
    this.grafHandler=this.grafHandler.bind(this);
  }

  
  
  createLegend(json) {
    var legend = [];
    for (var i = 0; i < json["names"].length; i++) {
      var type = "fa fa-circle text-" + json["types"][i];
      legend.push(<i className={type} key={i} />);
      legend.push(" ");
      legend.push(json["names"][i]);
    }
    return legend;
  }

  async componentWillMount(){
    const cookies= new Cookies;
    
    await this.setState({
      email:cookies.get('email'),
      token:cookies.get('token')
    })

    axios.defaults.headers.common['Authorization']='Bearer ' +this.state.token;
    await axios.get('https://rowingbackend.herokuapp.com/korisnik/dohvatiId/'+this.state.email + '.+').then(resp=>{
      this.setState({idKorisnika:resp.data})
    });
  }

  grafHandler(tren){
    var pom={
      rezultati:[],
      ocekivanoVrijeme:[],
      labels:[]
    }

    var donjaGranica=10;
    var gornjaGranica=0;

    for(var i=0; i<tren.trening.rezultat.length;i++){
      var rez=Math.floor(tren.trening.rezultat[i]/60)+ (tren.trening.rezultat[i]%60)/100
      pom.rezultati.push(rez)
    }

    for(var i=0;i < tren.vjezba.ocekivanoVrijeme.length;i++){
      var vrijeme=tren.vjezba.ocekivanoVrijeme[i]+tren.trening.vrijemeTest
      pom.ocekivanoVrijeme.push(Math.floor(vrijeme/60)+(vrijeme%60)/100)
      pom.labels.push(tren.vjezba.distance[i])
    }
    console.log(tren.vjezba.ocekivanoVrijeme)

    this.setState({dataSales : {
      labels: pom.labels,
      series: [
        pom.rezultati,
        pom.ocekivanoVrijeme,
      ],
    }})

    pom.rezultati.map(rez=>{
      if(rez>gornjaGranica){
        gornjaGranica=rez;
      }
      if(rez<donjaGranica){
        donjaGranica=rez
      }
    })

    pom.ocekivanoVrijeme.map(rez=>{
      if(rez>gornjaGranica){
        gornjaGranica=rez;
      }
      if(rez<donjaGranica){
        donjaGranica=rez
      }
    })
    console.log("Gornja: "+(parseFloat(gornjaGranica)+0.08))
    console.log("Donja: "+ (parseFloat(donjaGranica)-0.08))

    this.setState({
      optionsSales:{
        low: donjaGranica,
        high: gornjaGranica,
        showArea: false,
        height: "300px",
        axisX: {
          showGrid: false
        },
        lineSmooth: true,
        showLine: true,
        showPoint: true,
        fullWidth: true,
        chartPadding: {
          right: 50
        }
      }
    })
  }

  

  render() {    
    var cookies=new Cookies()
        if(cookies.get("token")===undefined){
            return <Redirect to="/"></Redirect>
        }
    return (
      <div className="content">
        <Grid fluid>
          
          <Row>
            <Col  md={8}>
              <Card
                statsIcon=""
                id="s"
                title=""
                category=""
                stats=""
                content={
                  <div className="ct-chart">
                    <ChartistGraph
                      data={this.state.dataSales}
                      type="Line"
                      options={this.state.optionsSales}
                      responsiveOptions={responsiveSales}
                    />
                  </div>
                }
                legend={
                  <div className="legend">{this.createLegend(legendSales)}</div>
                }
              />
            </Col>
            <Col md={4}>
              <Card
                title="Popis treninga"
                category=""
                stats=""
                content={
                  <div
                    id="chartPreferences"
                    className="ct-chart ct-perfect-fourth"
                  >
                    <PopisTreninga token={this.state.token} idKor={this.state.idKorisnika} grafHandler={this.grafHandler}/>
                  </div>
                }
              />
            </Col>
          </Row>

         
        </Grid>
      </div>
    );
  }
}

export default Dashboard;
